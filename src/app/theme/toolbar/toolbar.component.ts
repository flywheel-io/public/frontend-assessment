import { Component, EventEmitter, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { FwSnackbarService, FwSnackbarMessage } from '@flywheel-io/vision'

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent {
  @Output() themeToggle: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(public dialog: MatDialog, private snackbarService: FwSnackbarService) {
  }


  handleModeToggle(): void {
    this.themeToggle.emit(true);
  }

  addMessages(): void {
    const messages: FwSnackbarMessage[] = [
      { message: 'This is a info message', severity: 'info' },
      { message: 'This is a success message', severity: 'success' },
      { message: 'This is a warning message', severity: 'warning', actionText: 'retry', action: () => alert('retrty') },
      { message: 'This is an error message', severity: 'error' },
    ];
    this.snackbarService.show({
      ...messages[Math.floor(Math.random() * messages.length)],
    });
  }

}
