import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BehaviorSubject, combineLatest, map, Subject, Subscription, switchMap } from 'rxjs';
import { FwSnackbarService } from '@flywheel-io/vision';

import { ApiService, User } from '../../api.service';

class DataRow {
  id: string;
  name: string;
  email: string;
  role: string;
  status: string;
}

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.scss'],
})
export class UsersTableComponent implements OnInit, OnDestroy {
  @ViewChild('matSort') matSort = new MatSort();

  dataSource = new MatTableDataSource<DataRow>();
  displayedColumns: string[] = ['name', 'email', 'role', 'status'];
  selectedRow: DataRow = null;

  private subscriptions = {
    users: Subscription.EMPTY,
  }

  constructor(
    private api: ApiService,
    private snackbarService: FwSnackbarService,
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  ngOnDestroy(): void {
    for (const subscription of Object.values(this.subscriptions)) {
      subscription.unsubscribe();
    }
  }

  loadData(): void {
    this.subscriptions?.users.unsubscribe();
    this.subscriptions.users = this.api.users.query().subscribe({
      next: (users) => {
        this.selectedRow = users[0];
        this.dataSource.data = users;
        this.dataSource.sort = this.matSort;
      },
      error: (resp) => {
        console.error(resp);
        this.snackbarService.show({ message: resp.error, severity: 'error' });
      },
    });
  }

  rowSelected(row: DataRow): void {
    this.selectedRow = row;
  }

  getInitials(name: string): string {
    const names = name.split(' ');
    return names[0].substring(0, 1) + (names[1]?.substring(0, 1) || '');
  }
}
