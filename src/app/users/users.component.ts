import { Component } from '@angular/core';

import { FwDialogService } from '@flywheel-io/vision';
import { NewUserDialogComponent } from './new-user-dialog/new-user-dialog.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent {

  constructor(public dialog: FwDialogService) { }

  openNewUserDialog(): void {
    this.dialog.openDialog(NewUserDialogComponent, {
      data: {},
    });
  }

}
