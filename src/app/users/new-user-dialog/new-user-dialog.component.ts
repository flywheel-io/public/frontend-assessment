import { DIALOG_DATA, DialogRef } from '@angular/cdk/dialog';
import { Component, Inject } from '@angular/core';

import { DialogWidth } from '@flywheel-io/vision';

@Component({
  selector: 'app-new-user-dialog',
  templateUrl: './new-user-dialog.component.html',
  styleUrls: ['./new-user-dialog.component.scss'],
})
export class NewUserDialogComponent {
  protected readonly DialogWidth = DialogWidth;
  roles = [
    { title: 'User', value: 'user' },
    { title: 'Developer', value: 'developer' },
    { title: 'Admin', value: 'admin' },
  ];
  selectedRole = this.roles[0];

  constructor(
    public dialogRef: DialogRef,
    @Inject(DIALOG_DATA) public data?: DialogData,
  ) {
  }

  cancel(): void {
    this.dialogRef.close();
  }

  select(): void {
    this.dialogRef.close();
  }
}

export class DialogData {

}
